defmodule Caveatica do
  use Application

  @pins [17, 18, 27, 22]
  @steps [
    [0, 1, 1, 1],
    [0, 0, 1, 1],
    [1, 0, 1, 1],
    [1, 0, 0, 1],
    [1, 1, 0, 1],
    [1, 1, 0, 0],
    [1, 1, 1, 0],
    [0, 1, 1, 0]
  ]

  def start(_type, _args) do
    IO.puts "Caveatica.start"
    Gpio.Pwm.Stepper.start_link(%{pins: @pins, steps: @steps})
    {:ok, self()}
  end
end
