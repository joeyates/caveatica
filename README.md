# Caveatica - Chicken Coop Automation

* Latin: *cavea*, *-ae* (noun, f.) - cage, coop or enclosure.
* English: *caveatica* (noun) - chicken coop automation.

# Description

Remotely open and close a sliding door on a chicken coop.
View the inside of the coop via a webcam.

# System

* Raspberry Pi Zero (Pi3 for tests)
* Stepper motor, pulley, fish wire and sliding door
* Ethernet connection via Powerline
* Nerves operating system
* Elixir project:
  * motor control
  * web interface for opening and closing

# Stepper Motor Setup

## Texas Instruments ULN2003AN - Darlington Transistor

```
| GPIO  | GPIO     | ULN2003 | Colour   |
| Pin   | Name     | Name    |          |
| -----:|:-------- |:-------:|:-------- |
|    2  | +5v      | +       | Red      |
|    6  | GND      | -       | Brown    |
|   11  | GPIO 17  | IN1     | Orange   |
|   12  | GPIO 18  | IN2     | Yellow   |
|   13  | GPIO 27  | IN3     | Green    |
|   15  | GIPO 22  | IN4     | Blue     |
```

* [Raspberry Pi GPIO pins](https://en.wikipedia.org/wiki/Raspberry_Pi#General_purpose_input-output_(GPIO)_connector)

Jumpers: F - F

## 28BYJ-48 - Stepper Motor

* Monopolar
* Can be transformed into bipolar
* Voltage: 5V
* Max frequency: 100Hz
4076 steps per revolution

### Patterns

Half-step mode: 8 step control signal sequence

Clockwise steps:

```
| Colour | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|:------ | -:| -:| -:| -:| -:| -:| -:| -:|
| Red    | + | + | + | + | + | + | + | + |
| Orange | - | - | + | + | + | + | + | - |
| Yellow | + | - | - | - | + | + | + | + |
| Pink   | + | + | + | - | - | - | + | + |
| Blue   | + | + | + | + | + | - | - | - |
```

# Configuration

Create `config/production.secrets.exs` and set override configuration.

For an ethernet-connected device:

```elixir
config :nerves_init_gadget,
  node_name: :caveatica,
  mdns_domain: "caveatica.local",
  address_method: :dhcp,
  ifname: "eth0"
```

For wifi, see [nerves_network][nerves_network-wifi].

[nerves_network-wifi]: https://github.com/nerves-project/nerves_network#wifi-networking

# Development

## Dependencies

* libmnl-dev (for `nerves_network_interface`)

```shell
$ mix archive.install hex nerves_bootstrap
```

Install the Erlang version required by the build system.

## Environment

```shell
$ export MIX_TARGET=rpi3
```

## Installation

Create firmware:

```shell
$ mix firmware
```

Burn to an SD card:

```shell
$ mix firmware.burn
```

## Updates

```
$ mix firmware
$ ./upload.sh caveatica.local
```

## Debugging

```shell
$ ssh caveatica.local
> Gpio.Pwm.Stepper.start(:backwards, 1)
> RingLogger.next
```

Logout:
```shell
~.
```

# TODO

- [X] nerves project in repo
- [X] document build system
- [X] document hardware setup
- [X] library for stepper control
- [X] network access for Pi Zero
- [ ] web UI
- [ ] camera
