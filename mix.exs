defmodule Caveatica.Mixfile do
  use Mix.Project

  @target "rpi3"

  def project do
    [
      app: :caveatica,
      version: "0.1.0",
      elixir: "~> 1.6",
      target: @target,
      archives: [nerves_bootstrap: "~> 1.3"],
      deps_path: "deps/#{@target}",
      build_path: "_build/#{@target}",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [mod: {Caveatica, []},
     extra_applications: [:logger]]
  end

  def deps do
    [
      {:elixir_ale, "~> 1.2"},
      {:nerves, "~> 1.3.2", runtime: false},
      {:nerves_init_gadget, "~> 0.5"},
      {:nerves_leds, "~> 0.8.0"},
      {:nerves_network, "~> 0.3"},
      {:nerves_runtime, "~> 0.6"},
      {:nerves_system_rpi3, "~> 1.5.1", runtime: false},
      {:shoehorn, "~> 0.4"}
    ]
  end

  def aliases do
    [loadconfig: [&bootstrap/1]]
  end

  defp bootstrap(args) do
    Application.start(:nerves_bootstrap)
    Mix.Task.run("loadconfig", args)
  end
end
